use fake;

CREATE TABLE books (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100),
    author VARCHAR(100)
);

INSERT INTO books (title, author) VALUES 
    ('book1', 'author1'),
    ('book2', 'author2'),
    ('book3', 'author3'),
    ('book4', 'author4'),
    ('book5', 'author5');

GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';